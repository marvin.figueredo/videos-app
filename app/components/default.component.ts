import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'default',
  templateUrl: 'app/view/default.html',
  providers: [LoginService]
})

export class DefaultComponent { 
  public titulo = "Portada";
  public identity;
  
  constructor(
    private _loginService: LoginService
  ){ }
  
  ngOnInit(){
    this.identity = this._loginService.getIdentity();
    console.log(this.identity);
  }
}

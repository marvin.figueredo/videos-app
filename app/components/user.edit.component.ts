import { Component, OnInit } from '@angular/core';
import { User } from '../model/user';
import { LoginService } from '../services/login.service';
import { UploadService } from '../services/upload.service';
import { Router, ActivatedRoute } from '@angular/router'

@Component({
  selector: 'user-edit',
  templateUrl: 'app/view/user.edit.html',
  providers: [LoginService, UploadService]
})

export class UserEditComponent implements OnInit { 
  public titulo:string = "Actualizar mis datos";
  public user:User;
  public errorMessage;
  public status;

  constructor(
    private _loginService: LoginService,
    private _uploadService: UploadService,
    private _route: ActivatedRoute,
    private _router: Router
  ){ }

  ngOnInit(){
    let identity = this._loginService.getIdentity();
    if(identity == null){
        this._router.navigate(["/index"]);
    }
    else{
        this.user = new User(identity.sub, 
                            identity.role,
                            identity.name,
                            identity.surname,
                            identity.email,
                            identity.password,
                            "null");

    }
    
  }

  onSubmit(){
    this._loginService.update_user(this.user).subscribe(
      response => {
        this.status = response.status;

        if(this.status != "success"){
          console.log(response);
          this.status = "error";
        }
        else{
          localStorage.setItem('identity', JSON.stringify(this.user))
        }
      },
      error =>{
      	this.errorMessage = <any>error;
        if(this.errorMessage != null){
        	console.log(this.errorMessage);
        	alert("Error en la peticion");
        }
	  }); 
  }

  public filesToUpload: Array<File>;
  public resultUpload;

  fileChangeEvent(fileInput: any){
    this.filesToUpload = <Array<File>>fileInput.target.files;

    let token = this._loginService.getToken();
    
    this._uploadService.makeFileRequest(token, ['image'], this.filesToUpload).then(
      (result) => {
        this.resultUpload = result;
        console.log(this.resultUpload);
      },
      (error) =>{
        console.log(error);
      }
    );
  }

}
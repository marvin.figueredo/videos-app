import {Component, OnInit} from "@angular/core";
import { Router, ActivatedRoute} from "@angular/router";
import {UploadService} from "../services/upload.service";
import {LoginService} from "../services/login.service";
import {VideoService} from "../services/video.service";
import {User} from "../model/user";
import {Video} from "../model/video";

@Component({
    selector: "video-new",
    templateUrl: "app/view/video.new.html",
    providers: [LoginService, UploadService, VideoService]
})

export class VideoNewComponent implements OnInit{
    public titulo: string = "Crear un nuevo video";
    public video;
    public errorMessage;
    public status;
    public uploadedImage;

    constructor
    (
        private _loginService: LoginService,
        private _uploadService: UploadService,
        private _videoService: VideoService,
        private _route: ActivatedRoute,
        private _router: Router
    ){
        this.uploadedImage = false;
    }

    ngOnInit(){
        this.video = new Video(1,"", "", "public", null, null, null, null);
    }

    callVideoStatus(value){
        this.video.status = value;
    }

    onSubmit(){
        let token = this._loginService.getToken();
        
        this._videoService.create(token, this.video).subscribe(
            response => {
                this.status = response.status;
                console.log(response);
                if(this.status != "success"){
                    this.status = "error";
                }
                else{
                    this.video = response.data;
                    console.log(this.video);
                }
            },
            error =>{
                  this.errorMessage = <any>error;
                  if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la peticion");
                  }
            });
        }
/*
    public filesToUpload: Array<File>;
    public resultUpload;

    fileChangeEventImage(fileInput: any){
        this.filesToUpload = <Array<File>>fileInput.target.files;

    let token = this._loginService.getToken();
    
    this._uploadService.makeFileRequest(token, ['image'], this.filesToUpload).then(
      (result) => {
        this.resultUpload = result;
        console.log(this.resultUpload);
      },
      (error) =>{
        console.log(error);
      }
    );
    }*/
}

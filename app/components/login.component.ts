import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { Router, ActivatedRoute } from '@angular/router'

@Component({
  selector: 'login',
  templateUrl: 'app/view/login.html',
  providers: [LoginService]
})

export class LoginComponent implements OnInit { 
  public titulo: string = "Identifícate";
  public user;
  public errorMessage;
  public identity;
  public token;

  constructor(
    private _loginService: LoginService,
    private _route: ActivatedRoute,
    private _router: Router
  ){ }

  ngOnInit(){
    
    this._route.params.subscribe(params => {
      let logout = +params["id"];
      
      if (logout == 1){
        localStorage.removeItem('identity');
        localStorage.removeItem('token');
        this.identity = null;
        this.token = null;
        
        window.location.href = " /login";
      }
    });
    
    this.user = {
      "email": "",
      "password": "",
      "gethash": "false"
    };

    let identity = this._loginService.getIdentity();
    
    if (identity != null && identity.sub){
      this._router.navigate(["/index"]);
    }
  }

  onSubmit(){
    console.log(this.user);
    this.user.gethash = "false";
    this._loginService.signup(this.user).subscribe(
      response => {
        this.identity = response;

        if(this.identity.length <= 0){
          alert("Error en el servidor");
        }
        else{
          if(!this.identity.status){
            localStorage.setItem('identity', JSON.stringify(this.identity));
            
            this.user.gethash = "true";
            this._loginService.signup(this.user).subscribe(
                response => {
                  this.token = response;

                  if(this.token.lenght <= 0){
                    alert("Error en el servidor");
                  }
                  else{
                      if(!this.token.status){
                        localStorage.setItem('token', this.token);
                        window.location.href = "/";
                      }
                  }
                },
                error =>{
                  this.errorMessage = <any>error;
                  if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la peticion");
                  }
                });
          }
        }
      },
      error =>{
        this.errorMessage = <any>error;
        if(this.errorMessage != null){
          console.log(this.errorMessage);
          alert("Error en la peticion");
        }
      }
    );
  }
}

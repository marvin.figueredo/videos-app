import { Injectable } from "@angular/core";
import { Http, Response, Headers } from "@angular/http";
import "rxjs/add/operator/map";
import { Observable } from "rxjs/Observable";

@Injectable()
export class UploadService{
    public url = "http://127.0.0.1:8000/app_dev.php";

    constructor(private _http: Http){}

    makeFileRequest(token, params: Array<string>, files: Array<File>){
        return new Promise((resolve, reject) =>{
            var formData: any = new FormData();
            var xhr = new XMLHttpRequest();

            var name_file_input = params[0];

            for(var i=0; i<files.length; i++){
                formData.append(name_file_input, files[i], files[i].name);
            }

            formData.append("authorization", token);

            xhr.onreadystatechange = function(){
                if(xhr.readyState == 4){
                    if(xhr.status == 200){
                        resolve(JSON.parse(xhr.response));
                    }
                    else{
                        reject(xhr.response);                     
                    }
                }
            }

            var progress_div = document.getElementById("upload-progress-bar");
            progress_div.setAttribute("value", "0");
            progress_div.style.width = "0%";

            xhr.upload.addEventListener("progress", function(event: any){
                var progress_div = document.getElementById("upload-progress-bar");
                var percent = (event.loaded / event.total) * 100;
                let prc = Math.round(percent).toString();

                var status_div = document.getElementById("status");
                progress_div.setAttribute("value", prc);
                progress_div.style.width = prc + "%";
                status_div.innerHTML = Math.round(percent) + " % subido... por favor espera a que termine";
            }, false);

            xhr.upload.addEventListener("load", function(){
                let prc = "100";
                var progress_div = document.getElementById("upload-progress-bar");
                var status_div = document.getElementById("status");
                progress_div.setAttribute("value", prc);
                progress_div.setAttribute("aria-valuenow", prc);
                progress_div.style.width = prc + "%";
                status_div.innerHTML = "Subida completada";
                
            }, false);

            xhr.upload.addEventListener("error", function(){
                var status_div = document.getElementById("status");
                status_div.innerHTML = "Error en la subida";
            }, false);

            xhr.upload.addEventListener("abort", function(){
                var status_div = document.getElementById("status");
                status_div.innerHTML = "Subida abortada";
            }, false);

            xhr.open("POST", this.url + "/user/upload-image-user", true);
            xhr.send(formData);
        });
    }
}
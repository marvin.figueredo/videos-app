"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
var UploadService = (function () {
    function UploadService(_http) {
        this._http = _http;
        this.url = "http://127.0.0.1:8000/app_dev.php";
    }
    UploadService.prototype.makeFileRequest = function (token, params, files) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            var name_file_input = params[0];
            for (var i = 0; i < files.length; i++) {
                formData.append(name_file_input, files[i], files[i].name);
            }
            formData.append("authorization", token);
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            var progress_div = document.getElementById("upload-progress-bar");
            progress_div.setAttribute("value", "0");
            progress_div.style.width = "0%";
            xhr.upload.addEventListener("progress", function (event) {
                var progress_div = document.getElementById("upload-progress-bar");
                var percent = (event.loaded / event.total) * 100;
                var prc = Math.round(percent).toString();
                var status_div = document.getElementById("status");
                progress_div.setAttribute("value", prc);
                progress_div.style.width = prc + "%";
                status_div.innerHTML = Math.round(percent) + " % subido... por favor espera a que termine";
            }, false);
            xhr.upload.addEventListener("load", function () {
                var prc = "100";
                var progress_div = document.getElementById("upload-progress-bar");
                var status_div = document.getElementById("status");
                progress_div.setAttribute("value", prc);
                progress_div.setAttribute("aria-valuenow", prc);
                progress_div.style.width = prc + "%";
                status_div.innerHTML = "Subida completada";
            }, false);
            xhr.upload.addEventListener("error", function () {
                var status_div = document.getElementById("status");
                status_div.innerHTML = "Error en la subida";
            }, false);
            xhr.upload.addEventListener("abort", function () {
                var status_div = document.getElementById("status");
                status_div.innerHTML = "Subida abortada";
            }, false);
            xhr.open("POST", _this.url + "/user/upload-image-user", true);
            xhr.send(formData);
        });
    };
    return UploadService;
}());
UploadService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], UploadService);
exports.UploadService = UploadService;
//# sourceMappingURL=upload.service.js.map